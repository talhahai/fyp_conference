<?php

if (!isset($_SESSION))
	session_start();

require_once 'cfg.php';

$ScriptName = end(explode('/', $_SERVER['SCRIPT_NAME']));
if ($ScriptName == 'index.php')
	$NavAttr = 'class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar"';
else
	$NavAttr = 'class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1d4870;"';

$ScriptName = $ScriptName != 'index.php' ? 'index.php' : '';

?>
<nav <?php echo $NavAttr ?>>
	<div class="container">
		<a class="navbar-brand" href="index.php"><strong>UBIT Conference</strong></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!--Links-->
			<ul class="navbar-nav mr-auto smooth-scroll">
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#home">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#about">About</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#speakers">Speakers</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#call-for-papers">Call For Papers</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="schedule.php">Schedule</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#registration">Registration</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#sponsors">Sponsors</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="committee.php">Committee</a>
				</li>
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#contact">Contact</a>
				</li>
			</ul>
		</div>
	</div>
</nav>