<?php

if (!isset($_SESSION))
	session_start();

require_once 'admin/functions.php';

$About = Search_Query("SELECT * from about", true);
$DateGroups = Search_Query("SELECT * from schedule_date order by priority");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Schedule - <?php echo $About['Name'] ?></title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
</style>
</head>
<body>
	<header>
		<?php include 'nav.php'; ?>
	</header>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Schedule</h1>
				<h5><?php echo $About['Name'] ?></h5>
			</div>
		</div>

		<section id="sponsors" class="section mb-4">
			<div class="container pt-5 wow fadeIn" data-wow-delay="0.2s">
				<table class="table table-bordered">
					<tbody>
						<?php
						foreach ($DateGroups as $Group)
						{
							$ScheduleTimes = Search_Query("SELECT * from schedule_time where groupid = '".$Group['ID']."' order by priority");					if (empty($Members))
							if (empty($ScheduleTimes))
								continue;
							?>
							<tr class="mdb-color lighten-5">
								<th colspan="3"><strong><?php echo date('j M, Y', strtotime($Group['Date'])) ?></strong></th>
							</tr>
							<?php
							foreach ($ScheduleTimes as $Time)
							{
								?>
								<tr>
									<th style="width: 200px"><?php echo date('h:i A', strtotime($Time['StartTime'])).' - '.date('h:i A', strtotime($Time['EndTime'])) ?></th>
									<th><?php echo $Time['Text'] ?></th>
								</tr>
								<?php
							}
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="text-center">
				<a class="btn btn-lg btn-indigo wow fadeInUp waves-effect waves-light" data-wow-delay="0.2s">
					<span>Register Now</span>
				</a>
			</div>
		</section>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/jquery.particleground.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script>
		new WOW().init();
	</script>
</body>
</html>