-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 12, 2018 at 12:57 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ubit_conference`
--
CREATE DATABASE IF NOT EXISTS `ubit_conference` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ubit_conference`;

--
-- Truncate table before insert `about`
--

TRUNCATE TABLE `about`;
--
-- Dumping data for table `about`
--

INSERT INTO `about` (`ID`, `Name`, `DateFrom`, `DateTo`, `StartTime`, `Venue`) VALUES
(1, 'International Conference On Information Science & Communication Technology 2018', '2018-10-27', '2018-10-28', '08:00:00', 'Arts Auditorium, University of Karachi');

--
-- Truncate table before insert `about_para`
--

TRUNCATE TABLE `about_para`;
--
-- Dumping data for table `about_para`
--

INSERT INTO `about_para` (`ID`, `Text`, `Priority`) VALUES
(2, 'The Department of Computer Science, University of Karachi, was established by a resolution of Academic Council in its meeting, held on November 27. 1984, and it began functioning in the academic year 1985-86 by offering a Degree Program in Master of Computer Science (MCS) and become one of first institutions in Karachi imparting education in Computer Science and Technology. The Department also offers evening program leading to Post Graduate Diploma (PGD) in Computer & Information Sciences. In the year 1995; Department started MCS evening program, on self-finance basis, to cater the growing demand of professionally skilled manpower in the field of Computer Science.', 2),
(3, 'FCS at IBA endeavors to promote and uplift academic and organizational research activities in the discipline of computer science and information systems. Our aim is to provide a quality platform for acquiring, sharing, and disseminating results of completed and ongoing indigenous research activities. We hope to cultivate a strong tradition of applying state of the art research to the local contexts and solving local problems that can serve as global examples. To achieve this, we encourage and espouse collaborations with leading researchers around the world and keep exchange and growth of ideas in the prime focus.', 1),
(4, 'ICSIT is a biennial event and is entering its second decade of disseminating quality research in the area of Information and Communication Technologies. Topics of interest for submission include, but are not limited to artificial intelligence, robotics, social computing, wireless sensor networks, e-learning, computational science and other emerging topics. This year, the conference theme is "ICT in Everyday Life". We hope that you will enjoy the traditional Pakistani hospitality at ICSIT 2017, Karachi.', 3),
(5, 'komijm', 4);

--
-- Truncate table before insert `committee_groups`
--

TRUNCATE TABLE `committee_groups`;
--
-- Dumping data for table `committee_groups`
--

INSERT INTO `committee_groups` (`ID`, `Text`, `Priority`) VALUES
(1, 'Media Team', 1),
(2, 'Marketing Team', 2),
(3, 'Volunteers', 3),
(4, 'Logistics', 4);

--
-- Truncate table before insert `committee_members`
--

TRUNCATE TABLE `committee_members`;
--
-- Dumping data for table `committee_members`
--

INSERT INTO `committee_members` (`ID`, `GroupID`, `Name`, `Priority`) VALUES
(5, 1, 'Wasif', 1),
(6, 1, 'Hasan', 2),
(7, 1, 'Asifa', 3),
(8, 1, 'Ayaan', 4),
(9, 1, 'Nibras', 5),
(10, 1, 'Nasreen', 6),
(11, 2, 'Amna', 1),
(12, 2, 'Sara', 2),
(13, 2, 'Hira', 3),
(14, 2, 'Kiran', 5),
(15, 2, 'Areeba', 6),
(16, 2, 'Subhan', 4),
(17, 2, 'Faisal', 7),
(18, 3, 'Faiq', 1),
(19, 3, 'Shahvaiz', 2),
(20, 3, 'Usama', 3),
(21, 3, 'Afzal', 4),
(22, 4, 'Anas', 1),
(23, 4, 'Anwar', 2),
(24, 4, 'Lateef', 3);

--
-- Truncate table before insert `messages`
--

TRUNCATE TABLE `messages`;
--
-- Truncate table before insert `papers_list`
--

TRUNCATE TABLE `papers_list`;
--
-- Dumping data for table `papers_list`
--

INSERT INTO `papers_list` (`ID`, `Text`, `Priority`) VALUES
(1, 'Artificial Intelligence', 1),
(3, 'Pattern Recognition & Image Processing', 2),
(4, 'Computer Graphics & Computer Vision', 4),
(5, 'Social Network Analysis', 5),
(6, 'Modeling and Simulation', 6),
(7, 'Computer Networks and Data Communication', 7),
(8, 'Network Security', 8),
(9, 'Ubiquitous Computing', 9),
(10, 'Wireless Communication & Mobile Computing', 10),
(11, 'Internet of IT', 11),
(12, 'Information Systems', 12),
(13, 'Database Systems', 13),
(14, 'Software Engineering & CASE', 14),
(15, 'Theoretical Computer Science', 15),
(16, 'Data Mining', 3);

--
-- Truncate table before insert `papers_para`
--

TRUNCATE TABLE `papers_para`;
--
-- Dumping data for table `papers_para`
--

INSERT INTO `papers_para` (`ID`, `Text`, `Priority`) VALUES
(1, 'The international Conference on Information Sciences and Communication Technology (ICISCT) is the premier forum for the presentation of technological advances and research result in the field of theoretical experimental, and applied computer science and information technology. ICISCT 2018 will bring together leading scientist and engineers in the computer science and information technology from all around the world. Researchers are invited to submit their original, unpublished manuscript, which demonstrates current research in the area of Information Science and Technology.', 1);

--
-- Truncate table before insert `schedule_date`
--

TRUNCATE TABLE `schedule_date`;
--
-- Dumping data for table `schedule_date`
--

INSERT INTO `schedule_date` (`ID`, `Date`, `Priority`) VALUES
(1, '2018-10-27', 1),
(2, '2018-10-28', 2);

--
-- Truncate table before insert `schedule_time`
--

TRUNCATE TABLE `schedule_time`;
--
-- Dumping data for table `schedule_time`
--

INSERT INTO `schedule_time` (`ID`, `GroupID`, `StartTime`, `EndTime`, `Text`, `Priority`) VALUES
(5, 1, '10:00:00', '12:00:00', 'Recitation', 2),
(6, 1, '12:00:00', '13:00:00', 'Lunch', 1),
(7, 1, '13:00:00', '16:00:00', 'Speakers', 3),
(8, 2, '12:00:00', '14:00:00', 'Break', 1),
(9, 2, '16:00:00', '20:00:00', 'Discussion', 2);

--
-- Truncate table before insert `speakers`
--

TRUNCATE TABLE `speakers`;
--
-- Dumping data for table `speakers`
--

INSERT INTO `speakers` (`ID`, `Name`, `Organization`, `Education`, `Image`, `Biography`, `Priority`) VALUES
(1, 'Dr. M. Sadiq Ali Khan', 'Organization 1', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', 'images/speakers/1.png', 'Dr. M. Sadiq Ali Khan is Chairman at Department of Computer Science â€“ UBIT. He completed his Ph.D in 2011 under the supervision of Prof. Dr. S.M. Aqil Burney.', 1),
(2, 'Mr. Badar Sami', 'Organization 1', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', 'images/speakers/2.png', 'Mr. Badar Sami is Assistant Professor of Department of Computer Science â€“ UBIT. He is one of the best teachers of the UBIT. According to Mr. Badar Sami, the goal of the education is to have a peaceful, vibrant society, so that we can interact with other people in a peaceful manner.', 2),
(3, 'Prof Dr S.M. Aqil Burney', 'Organization 1', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', 'images/speakers/3.png', 'Prof Dr S.M. Aqil Burney has served well over 42 years at University of Karachi. Now Dr. Aqil Burney is Professor at College of Computer Science and Information Systems (CCSIS) at Institute of Business Management (IoBM)Karachi, one of the leading Business Schools of the country.', 3),
(4, 'Mr. Syed Jamal Hussain', 'Organization 1', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', 'images/speakers/4.png', 'Mr. Syed Jamal Hussain is Assistant Professor at Department of Computer and his hard-work for this department can never be neglected. He is one of the senior teachers who is eager his knowledge and experiences with the students.', 4),
(5, 'Ms. Maryam Feroze', 'Organization 1', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', 'images/speakers/5.png', 'She started teaching at Department of Computer Science â€“ UBIT in January 2008 as visiting faculty and now she is lecturer here.', 5),
(6, 'Mr. Usman Amjad', 'Organization 1', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', 'images/speakers/na-male.png', '', 7),
(7, 'Ms. Aemon Abdul Razzque', '', '', 'images/speakers/na-female.png', '', 6);

--
-- Truncate table before insert `sponsor_groups`
--

TRUNCATE TABLE `sponsor_groups`;
--
-- Dumping data for table `sponsor_groups`
--

INSERT INTO `sponsor_groups` (`ID`, `Text`, `Priority`) VALUES
(1, 'Platinum Sponsors', 1),
(2, 'Gold Sponsors', 2),
(3, 'Silver Sponsors', 3);

--
-- Truncate table before insert `sponsor_items`
--

TRUNCATE TABLE `sponsor_items`;
--
-- Dumping data for table `sponsor_items`
--

INSERT INTO `sponsor_items` (`ID`, `GroupID`, `Logo`, `Priority`) VALUES
(1, 1, 'images/sponsors/1.png', 1),
(2, 1, 'images/sponsors/2.png', 2),
(3, 1, 'images/sponsors/3.png', 3),
(4, 1, 'images/sponsors/4.png', 4),
(5, 2, 'images/sponsors/5.png', 1),
(6, 2, 'images/sponsors/6.png', 2),
(7, 2, 'images/sponsors/7.png', 3),
(9, 3, 'images/sponsors/9.png', 1);

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Username`, `Password`) VALUES
(1, '1', 'c4ca4238a0b923820dcc509a6f75849b'),
(2, 'master', 'eb0a191797624dd3a48fa681d3061212');
SET FOREIGN_KEY_CHECKS=1;
