<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

require_once 'functions.php';

$SponsorGroups = Search_Query("SELECT * from sponsor_groups order by priority");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title>Sponsors – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Sponsors</h1>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Sponsor Groups</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit" id="btnAddGroup">ADD GROUP</a>
				</div>
			</div>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold">Title</th>
							<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
							<th class="text-center font-weight-bold" style="width: 50px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($SponsorGroups as $Group)
						{
							?>
							<tr>
								<td><?php echo $Group['Text'] ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="sponsor_groups" data-action="up" data-id="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="sponsor_groups" data-action="down" data-id="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
								</td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="sponsor_groups" data-id="<?php echo $Group['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<?php

			$Script = '';
			foreach ($SponsorGroups as $Group)
			{
				$Sponsors = Search_Query("SELECT * from sponsor_items where groupid = '".$Group['ID']."' order by priority");
				$Script .= '$("#btnAddSponsor'.$Group['ID'].'").click(function() {
					$(".headingEntity").html("'.$Group['Text'].'");
					$("#inputID").val("'.$Group['ID'].'");
				});';
				?>
				<div class="row mt-4">
					<div class="col-6 align-self-center">
						<h4 class="green-color my-4"><?php echo $Group['Text'] ?></h4>
					</div>
					<div class="col-6 pull-right align-self-center">
						<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit2" id="btnAddSponsor<?php echo $Group['ID'] ?>">ADD SPONSOR</a>
					</div>
				</div>
				<div class="card">
					<table class="table">
						<thead>
							<tr>
								<th class="font-weight-bold">Logo</th>
								<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
								<th class="text-center font-weight-bold" style="width: 50px">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php

							foreach ($Sponsors as $Sponsor)
							{
								?>
								<tr>
									<td><img src="../<?php echo $Sponsor['Logo'] ?>" class="img-fluid" style="max-height: 100px"></td>
									<td class="align-middle text-center action-btn-2 p-0">
										<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-sponsor-priority" data-entity="sponsor_items" data-action="up" data-id="<?php echo $Sponsor['ID'] ?>" data-groupid="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
										<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-sponsor-priority" data-entity="sponsor_items" data-action="down" data-id="<?php echo $Sponsor['ID'] ?>" data-groupid="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
									</td>
									<td class="align-middle text-center action-btn-2 p-0">
										<a class="m-0 btn-sm btn-floating danger-color-dark btn-sponsor-delete waves-effect waves-light" data-entity="sponsor_items" data-id="<?php echo $Sponsor['ID'] ?>" data-groupid="<?php echo $Group['ID'] ?>"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<?php
			}
			?>

			<div class="modal fade" id="modalAddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add Sponsor Group</h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-groups">
							<div class="modal-body mb-0">
								<input type="hidden" id="inputEntity" name="entity" value="sponsor_groups">
								<div class="md-form">
									<input type="text" name="text" id="text" class="form-control" required>
									<label for="text">Title</label>
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-group">ADD Sponsor Group</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modalAddEdit2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add <span class="headingEntity">Sponsor</span></h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-sponsor">
							<div class="modal-body mb-0">
								<input type="hidden" id="inputID" name="groupid" value="0">
								<label>Logo</label>
								<div class="md-form">
									<input type="file" id="image" name="image" class="formcontrol" required>
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-sponsor">ADD <span class="headingEntity">SPONSOR</span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript" src="../js/bootbox.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			<?php 
			if (isset($_SESSION['toast-message']))
			{
				echo "toastr.success('".$_SESSION['toast-message']."', '', {positionClass: 'toast-bottom-left'});";
				unset($_SESSION['toast-message']);
			}
			?>

			$(".btn-priority").click(function() {
				var SelectedItem = $(this);
				var BtnHTML = SelectedItem.html();
				SelectedItem.prop('disabled', 'disabled')
				SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
				$.ajax({
					type: "get",
					url: "updatepriority.php",
					data: "id="+SelectedItem.data('id')+"&action="+SelectedItem.data('action')+"&entity="+SelectedItem.data('entity'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						SelectedItem.prop('disabled', '')
						SelectedItem.html(BtnHTML);
					}
				});
			});

			$(".btn-sponsor-priority").click(function() {
				var SelectedItem = $(this);
				var BtnHTML = SelectedItem.html();
				SelectedItem.prop('disabled', 'disabled')
				SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
				$.ajax({
					type: "get",
					url: "updatesponsorpriority.php",
					data: "id="+SelectedItem.data('id')+"&action="+SelectedItem.data('action')+"&entity="+SelectedItem.data('entity')+"&groupid="+SelectedItem.data('groupid'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						SelectedItem.prop('disabled', '')
						SelectedItem.html(BtnHTML);
					}
				});
			});

			$(".btn-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this item?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deleteentity.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});

			$(".btn-sponsor-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this item?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deletesponsor.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity')+"&groupid="+SelectedItem.data('groupid'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});

			<?php echo($Script) ?>

			$("#form-groups").submit(function(e) {
				e.preventDefault();
				$("#add-group").prop('disabled', 'disabled')
				$("#add-group").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING GROUP');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "addparagraph.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Group added successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-groups")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-group").prop('disabled', '')
						$("#add-group").html('ADD Group');
					}
				});
			});

			$("#form-sponsor").submit(function(e) {
				e.preventDefault();
				$("#add-sponsor").prop('disabled', 'disabled')
				$("#add-sponsor").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING <span class="headingEntity">'+$(".headingEntity").html()+'</span>');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "addsponsor.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success($(".headingEntity").html()+' added successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-sponsor")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-sponsor").prop('disabled', '')
						$("#add-sponsor").html('ADD <span class="headingEntity">'+$(".headingEntity").html()+'</span>');
					}
				});
			});
		});
	</script>
</body>
</html>