<?php

require_once 'functions.php';

if (isset($_GET['entity']) && isset($_GET['id']) && isset($_GET['action']))
{
	if (!empty($_GET['entity']) && !empty($_GET['id']) && !empty($_GET['action']))
	{
		$CurrentPriority = Search_Query("SELECT Priority from ".$_GET['entity']." where id = '".$_GET['id']."'")[0]['Priority'];
		$MaxPriority = Search_Query("SELECT max(priority) as 'MaxPriority' from ".$_GET['entity']."")[0]['MaxPriority'];

		if ($CurrentPriority == 1 && $_GET['action'] == 'up')
		{
			echo 'true';
			return;
		}
		if ($CurrentPriority == $MaxPriority && $_GET['action'] == 'down')
		{
			echo 'true';
			return;
		}

		if ($_GET['action'] == 'up')
		{
			mysqli_query($Connection, "UPDATE ".$_GET['entity']." set priority = priority + 1 where priority = '".($CurrentPriority - 1)."'");
			mysqli_query($Connection, "UPDATE ".$_GET['entity']." set priority = priority - 1 where id = '".$_GET['id']."'");

			if (!isset($_SESSION))
				session_start();

			unset($_SESSION['toast-message']);
			$_SESSION['toast-message'] = 'Priority updated successfully';

			echo 'true';
		}
		else if ($_GET['action'] == 'down')
		{ 
			mysqli_query($Connection, "UPDATE ".$_GET['entity']." set priority = priority - 1 where priority = '".($CurrentPriority + 1)."'");
			mysqli_query($Connection, "UPDATE ".$_GET['entity']." set priority = priority + 1 where id = '".$_GET['id']."'");

			if (!isset($_SESSION))
				session_start();

			unset($_SESSION['toast-message']);
			$_SESSION['toast-message'] = 'Priority updated successfully';
			
			echo 'true';
		}
	}
}

?>