<?php

require_once 'functions.php';

if (isset($_POST['groupid']) && isset($_POST['start_time']) && isset($_POST['end_time']) && isset($_POST['text']))
{
	if (!empty($_POST['groupid']) && !empty($_POST['start_time']) && !empty($_POST['end_time']) && !empty($_POST['text']))
	{
		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from schedule_time where groupid = '".$_POST['groupid']."'", true)['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO schedule_time set
			groupid = '".mysqli_real_escape_string($Connection, $_POST['groupid'])."',
			starttime = '".date('H:i:s', strtotime($_POST['start_time']))."',
			endtime = '".date('H:i:s', strtotime($_POST['end_time']))."',
			text = '".mysqli_real_escape_string($Connection, $_POST['text'])."',
			Priority = '".$MaxPriority."'"))
		{
			echo 'true';
			return;
		}
		else
		{	
			echo "Time not added, please try again later";
			return;
		}
	}
}

echo "Time not added, try to fill required fields";
return;

?>