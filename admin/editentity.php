<?php

require_once 'functions.php';

if (isset($_POST['id']) && isset($_POST['entity']) && isset($_POST['description']))
{
	if (!empty($_POST['id']) && !empty($_POST['entity']) && !empty($_POST['description']))
	{
		if (in_array($_POST['entity'], ['educations', 'awards', 'interests']))
		{
			if(mysqli_query($Connection, "UPDATE ".$_POST['entity']." set
				description = '".mysqli_real_escape_string($Connection, $_POST['description'])."'
				where id = '".$_POST['id']."'"))
			{
				echo 'true';
				return;
			}
			else
			{	
				echo ucwords($_POST['entity'])." not edited, please try again later";
				return;
			}
		}
	}
}

echo ucwords($_POST['entity'])." not edited, try to fill required fields";
return;

?>