<?php

$Connection = mysqli_connect('localhost', 'web_gwadar', 'Webdroid@2901', 'ubit_conference'); //server
//$Connection = mysqli_connect('localhost', 'root', '', 'conference'); //local
if (!$Connection || mysqli_errno($Connection))
	die('Can not connect to database.');

date_default_timezone_set("Asia/Karachi");

function Search_Query($Query, $FirstRow = false, $PrintError = false)
{
	global $Connection;
	$QueryResult = mysqli_query($Connection, $Query);

	if ($PrintError == true)
	{
		echo mysqli_error($Connection);
	}

	$Result = array();
	if (mysqli_num_rows($QueryResult) > 0)
	{
		$i = 0;
		while($Result[$i] = mysqli_fetch_assoc($QueryResult))
			$i++;
		unset($Result[$i]);

		if ($FirstRow)
			return $Result[0];
	}
	
	return $Result;
}

?>