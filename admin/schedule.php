<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

require_once 'functions.php';

$DateGroups = Search_Query("SELECT * from schedule_date order by priority");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title>Schedule – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Schedule</h1>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Event Days</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit" id="btnAddGroup">ADD EVENT DAY</a>
				</div>
			</div>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold">Date</th>
							<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
							<th class="text-center font-weight-bold" style="width: 50px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($DateGroups as $Group)
						{
							?>
							<tr>
								<td><?php echo date('j M, Y', strtotime($Group['Date'])) ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="schedule_date" data-action="up" data-id="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="schedule_date" data-action="down" data-id="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
								</td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="schedule_date" data-id="<?php echo $Group['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<?php

			$Script = '';
			foreach ($DateGroups as $Group)
			{
				$ScheduleTimes = Search_Query("SELECT * from schedule_time where groupid = '".$Group['ID']."' order by priority");
				$Script .= '$("#btnAddTime'.$Group['ID'].'").click(function() {
					$(".headingEntity").html("'.date('j M, Y', strtotime($Group['Date'])).'");
					$("#inputID").val("'.$Group['ID'].'");
				});';
				?>
				<div class="row mt-4">
					<div class="col-6 align-self-center">
						<h4 class="green-color my-4">Programme for <?php echo date('j M, Y', strtotime($Group['Date'])) ?></h4>
					</div>
					<div class="col-6 pull-right align-self-center">
						<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit2" id="btnAddTime<?php echo $Group['ID'] ?>">ADD PROGRAMME</a>
					</div>
				</div>
				<div class="card">
					<table class="table">
						<thead>
							<tr>
								<th class="font-weight-bold">Time</th>
								<th class="font-weight-bold">Programme</th>
								<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
								<th class="text-center font-weight-bold" style="width: 50px">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php

							foreach ($ScheduleTimes as $Time)
							{
								?>
								<tr>
									<td><?php echo date('h:i A', strtotime($Time['StartTime'])).' - '.date('h:i A', strtotime($Time['EndTime'])) ?></td>
									<td><?php echo $Time['Text'] ?></td>
									<td class="align-middle text-center action-btn-2 p-0">
										<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-time-priority" data-entity="schedule_time" data-action="up" data-id="<?php echo $Time['ID'] ?>" data-groupid="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
										<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-time-priority" data-entity="schedule_time" data-action="down" data-id="<?php echo $Time['ID'] ?>" data-groupid="<?php echo $Group['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
									</td>
									<td class="align-middle text-center action-btn-2 p-0">
										<a class="m-0 btn-sm btn-floating danger-color-dark btn-time-delete waves-effect waves-light" data-entity="schedule_time" data-id="<?php echo $Time['ID'] ?>" data-groupid="<?php echo $Group['ID'] ?>"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<?php
			}
			?>

			<div class="modal fade" id="modalAddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add Event Day</h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-day">
							<div class="modal-body mb-0">
								<div class="md-form">
									<input type="date" name="date" id="date" class="form-control" required>
									<label for="date" class="active">Date</label>
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-day">Add Day</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modalAddEdit2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add Programme for <span class="headingEntity">2018-03-03</span></h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-time">
							<div class="modal-body mb-0">
								<input type="hidden" id="inputID" name="groupid" value="0">
								<div class="md-form">
									<input type="time" name="start_time" id="start_time" class="form-control" required>
									<label for="start_time" class="active">Start Time</label>
								</div>
								<div class="md-form">
									<input type="time" name="end_time" id="end_time" class="form-control" required>
									<label for="end_time" class="active">End Time</label>
								</div>
								<div class="md-form">
									<input type="text" name="text" id="text" class="form-control" required>
									<label for="text">Programme</label>
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-time">Add Programme</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript" src="../js/bootbox.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			<?php 
			if (isset($_SESSION['toast-message']))
			{
				echo "toastr.success('".$_SESSION['toast-message']."', '', {positionClass: 'toast-bottom-left'});";
				unset($_SESSION['toast-message']);
			}
			?>

			$(".btn-priority").click(function() {
				var SelectedItem = $(this);
				var BtnHTML = SelectedItem.html();
				SelectedItem.prop('disabled', 'disabled')
				SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
				$.ajax({
					type: "get",
					url: "updatepriority.php",
					data: "id="+SelectedItem.data('id')+"&action="+SelectedItem.data('action')+"&entity="+SelectedItem.data('entity'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						SelectedItem.prop('disabled', '')
						SelectedItem.html(BtnHTML);
					}
				});
			});

			$(".btn-time-priority").click(function() {
				var SelectedItem = $(this);
				var BtnHTML = SelectedItem.html();
				SelectedItem.prop('disabled', 'disabled')
				SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
				$.ajax({
					type: "get",
					url: "updatesponsorpriority.php",
					data: "id="+SelectedItem.data('id')+"&action="+SelectedItem.data('action')+"&entity="+SelectedItem.data('entity')+"&groupid="+SelectedItem.data('groupid'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						SelectedItem.prop('disabled', '')
						SelectedItem.html(BtnHTML);
					}
				});
			});

			$(".btn-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this item?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deleteentity.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});

			$(".btn-time-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this item?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deletesponsor.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity')+"&groupid="+SelectedItem.data('groupid'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});

			<?php echo($Script) ?>

			$("#form-day").submit(function(e) {
				e.preventDefault();
				$("#add-day").prop('disabled', 'disabled')
				$("#add-day").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING DAY');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "addday.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Event day added successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-day")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-day").prop('disabled', '')
						$("#add-day").html('ADD DAY');
					}
				});
			});

			$("#form-time").submit(function(e) {
				e.preventDefault();
				$("#add-time").prop('disabled', 'disabled')
				$("#add-time").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING PROGRAMME');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "addprogramme.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Programme added successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-time")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-time").prop('disabled', '')
						$("#add-time").html('ADD PROGRAMME');
					}
				});
			});
		});
	</script>
</body>
</html>