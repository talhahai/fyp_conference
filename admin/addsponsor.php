<?php

require_once 'functions.php';

if (isset($_POST['groupid']) && isset($_FILES["image"]["name"]))
{
	if (!empty($_POST['groupid']) && !empty($_FILES["image"]["name"]))
	{
		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from sponsor_items where groupid = '".$_POST['groupid']."'", true)['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO sponsor_items set
			groupid = '".mysqli_real_escape_string($Connection, $_POST['groupid'])."',
			logo = 'todo',
			Priority = '".$MaxPriority."'"))
		{
			$SponsorID = mysqli_insert_id($Connection);

			$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

			if ($FileExtension != 'png' && $FileExtension != 'jpg' && $FileExtension != 'jpeg')
			{
				echo "File format not suported";
				return;
			}

			$FileName = 'images/sponsors/'.$SponsorID.'.'.$FileExtension;

			if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
			{
				mysqli_query($Connection, "UPDATE sponsor_items set Logo = '".$FileName."' where id = '".$SponsorID."'");
				echo 'true';
				return;
			}
			else
			{
				mysqli_query($Connection, "DELETE FROM sponsor_items where id = '".$SponsorID."'");
				echo "Sponsor not added, please try again later";
				return;
			}
		}
		else
		{	
			echo "Sponsor not added, please try again later";
			return;
		}
	}
}

echo "Sponsor not added, try to fill required fields";
return;

?>