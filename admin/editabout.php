<?php

require_once 'functions.php';

if (isset($_POST['name']) && isset($_POST['venue']) && isset($_POST['date_from']) && isset($_POST['start_time']) && isset($_POST['date_to']))
{
	if (!empty($_POST['name']) && !empty($_POST['venue']) && !empty($_POST['date_from']) && !empty($_POST['start_time']) && !empty($_POST['date_to']))
	{
		if(mysqli_query($Connection, "UPDATE about set
			name = '".mysqli_real_escape_string($Connection, $_POST['name'])."',
			venue = '".mysqli_real_escape_string($Connection, $_POST['venue'])."',
			datefrom = '".date('Y-m-d', strtotime($_POST['date_from']))."',
			starttime = '".date('H:i:s', strtotime($_POST['start_time']))."',
			dateto = '".date('Y-m-d', strtotime($_POST['date_to']))."'"))
		{
			echo 'true';
			return;
		}
		else
		{	
			echo "Data not edited, please try again later";
			return;
		}
	}
}

echo "Data not edited, try to fill required fields";
return;

?>