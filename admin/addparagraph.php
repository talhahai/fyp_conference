<?php

require_once 'functions.php';

if (isset($_POST['text']) && isset($_POST['entity']))
{
	if (!empty($_POST['text']) && !empty($_POST['entity']))
	{
		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from ".$_POST['entity'], true)['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO ".$_POST['entity']." set
			text = '".mysqli_real_escape_string($Connection, $_POST['text'])."',
			Priority = '".$MaxPriority."'"))
		{
			echo 'true';
			return;
		}
		else
		{	
			echo ucwords($_POST['entity'])." not added, please try again later";
			return;
		}
	}
}

echo ucwords($_POST['entity'])." not added, try to fill required fields";
return;

?>