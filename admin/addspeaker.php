<?php

require_once 'functions.php';

if (isset($_POST['name']) && isset($_POST['organization']) && isset($_POST['education']) && isset($_POST['bio']) && isset($_POST['gender']))
{
	if (!empty($_POST['name']) && !empty($_POST['gender']))
	{
		if ($_POST['gender'] == 'male')
			$FileName = 'images/speakers/na-male.png';
		else
			$FileName = 'images/speakers/na-female.png';

		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from speakers")[0]['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO speakers set
			name = '".mysqli_real_escape_string($Connection, $_POST['name'])."',
			organization = '".mysqli_real_escape_string($Connection, $_POST['organization'])."',
			education = '".mysqli_real_escape_string($Connection, $_POST['education'])."',
			biography = '".mysqli_real_escape_string($Connection, $_POST['bio'])."',
			Image = '".$FileName."',
			Priority = '".$MaxPriority."'"))
		{
			$SpeakerID = mysqli_insert_id($Connection);

			if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"]))
			{
				$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

				if ($FileExtension != 'png' && $FileExtension != 'jpg' && 
					$FileExtension != 'jpeg' && $FileExtension != 'zip')
				{
					echo "File format not suported";
					return;
				}

				$FileName = 'images/speakers/'.$SpeakerID.'.'.$FileExtension;

				if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
				{
					mysqli_query($Connection, "UPDATE speakers set Image = '".$FileName."' where id = '".$SpeakerID."'");
				}
			}

			echo 'true';
			return;
		}
		else
		{	
			echo "Speaker not added, please try again later";
			return;
		}
	}
}

echo "Speaker not added, try to fill required fields";
return;

?>