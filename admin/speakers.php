<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

require_once 'functions.php';

$Speakers = Search_Query("SELECT * from speakers order by priority");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title>Speakers – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Speakers</h1>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Speakers</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit" id="btnAddSpeaker">ADD SPEAKER</a>
				</div>
			</div>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold" style="width: 100px">Picture</th>
							<th class="font-weight-bold">Name</th>
							<th class="font-weight-bold">Organization</th>
							<th class="font-weight-bold">Education</th>
							<th class="font-weight-bold">Biography</th>
							<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
							<th class="text-center font-weight-bold" style="width: 50px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($Speakers as $Speaker)
						{
							?>
							<tr>
								<td><img src="../<?php echo $Speaker['Image'] ?>" class="img-fluid" style="max-height: 100px"></td>
								<td><?php echo $Speaker['Name'] ?></td>
								<td><?php echo $Speaker['Organization'] ?></td>
								<td><?php echo $Speaker['Education'] ?></td>
								<td><?php echo $Speaker['Biography'] ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="speakers" data-action="up" data-id="<?php echo $Speaker['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="speakers" data-action="down" data-id="<?php echo $Speaker['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
								</td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="speakers" data-id="<?php echo $Speaker['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<div class="modal fade" id="modalAddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add Speaker</h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-speaker">
							<div class="modal-body mb-0">
								<div class="md-form">
									<input type="text" id="name" name="name" class="form-control" required>
									<label for="name">Name</label>
								</div>
								<div class="md-form">
									<input type="text" id="organization" name="organization" class="form-control">
									<label for="organization">Organization</label>
								</div>
								<div class="md-form">
									<input type="text" id="education" name="education" class="form-control">
									<label for="education">Education</label>
								</div>
								<div class="md-form">
									<textarea type="text" name="bio" id="bio" class="md-textarea" rows="3" style="height: auto;"></textarea>
									<label for="bio">Biography</label>
								</div>
								<label class="d-block">Gender</label>
								<select name="gender" class="mdb-select">
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
								<div class="md-form">
									<label class="d-block" style="position: relative; top: initial;">Picture</label>
									<input type="file" id="image" name="image" class="formcontrol">
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-speaker">ADD SPEAKER</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript" src="../js/bootbox.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			<?php 
			if (isset($_SESSION['toast-message']))
			{
				echo "toastr.success('".$_SESSION['toast-message']."', '', {positionClass: 'toast-bottom-left'});";
				unset($_SESSION['toast-message']);
			}
			?>

			$(".btn-priority").click(function() {
				var SelectedItem = $(this);
				var BtnHTML = SelectedItem.html();
				SelectedItem.prop('disabled', 'disabled')
				SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
				$.ajax({
					type: "get",
					url: "updatepriority.php",
					data: "id="+SelectedItem.data('id')+"&action="+SelectedItem.data('action')+"&entity="+SelectedItem.data('entity'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						SelectedItem.prop('disabled', '')
						SelectedItem.html(BtnHTML);
					}
				});
			});

			$(".btn-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this speaker?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deleteentity.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});

			$("#form-speaker").submit(function(e) {
				e.preventDefault();
				$("#add-speaker").prop('disabled', 'disabled')
				$("#add-speaker").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING speaker</span>');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "addspeaker.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Speaker added successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-speaker")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-speaker").prop('disabled', '')
						$("#add-speaker").html('ADD speaker');
					}
				});
			});
		});
	</script>
</body>
</html>