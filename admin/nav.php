<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

$ScriptName = end(explode('/', $_SERVER['SCRIPT_NAME']));

?>
<nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1d4870;">
	<div class="container">
		<a class="navbar-brand" href="index.php">
			UBIT Conference
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav smooth-scroll ml-auto nav-flex-icons">
				<li class="nav-item">
					<a class="nav-link" href="index.php">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="speakers.php">Speakers</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="papers.php">Call For Papers</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="sponsors.php">Sponsors</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="committee.php">Committee</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="schedule.php">Schedule</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="messages.php">Messages</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="logout.php">Logout</a>
				</li>
			</ul>
		</div>
	</div>
</nav>