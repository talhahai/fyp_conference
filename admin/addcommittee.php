<?php

require_once 'functions.php';

if (isset($_POST['groupid']) && isset($_POST['name']))
{
	if (!empty($_POST['groupid']) && !empty($_POST['name']))
	{
		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from committee_members where groupid = '".$_POST['groupid']."'", true)['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO committee_members set
			groupid = '".mysqli_real_escape_string($Connection, $_POST['groupid'])."',
			name = '".mysqli_real_escape_string($Connection, $_POST['name'])."',
			Priority = '".$MaxPriority."'"))
		{
			echo 'true';
			return;
		}
		else
		{	
			echo "Member not added, please try again later";
			return;
		}
	}
}

echo "Member not added, try to fill required fields";
return;

?>