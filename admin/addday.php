<?php

require_once 'functions.php';

if (isset($_POST['date']))
{
	if (!empty($_POST['date']))
	{
		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from schedule_date", true)['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO schedule_date set
			date = '".date('Y-m-d', strtotime($_POST['date']))."',
			Priority = '".$MaxPriority."'"))
		{
			echo 'true';
			return;
		}
		else
		{	
			echo "Date not added, please try again later";
			return;
		}
	}
}

echo "Date not added, try to fill required fields";
return;

?>