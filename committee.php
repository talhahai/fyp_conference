<?php

if (!isset($_SESSION))
	session_start();

require_once 'admin/functions.php';

$About = Search_Query("SELECT * from about", true);
$CommitteeGroups = Search_Query("SELECT * from committee_groups order by priority");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Committee - <?php echo $About['Name'] ?></title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
</style>
</head>
<body>
	<header>
		<?php include 'nav.php'; ?>
	</header>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Committee</h1>
				<h5><?php echo $About['Name'] ?></h5>
			</div>
		</div>

		<section id="sponsors" class="section mb-2">
			<div class="container py-5 wow fadeIn" data-wow-delay="0.2s">		
				<?php
				foreach ($CommitteeGroups as $Group)
				{
					$Members = Search_Query("SELECT * from committee_members where groupid = '".$Group['ID']."' order by priority");
					if (empty($Members))
						continue;
					?>
					<h5 class="font-bold mb-1"><?php echo $Group['Text'] ?></h5>
					<ul class="mt-2">
						<?php
						foreach ($Members as $Member)
							echo '<li>'.$Member['Name'].'</li>';
						?>
					</ul>
					<?php
				}
				?>
			</div>
		</section>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/jquery.particleground.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script>
		new WOW().init();
	</script>
</body>
</html>