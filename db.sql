-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2018 at 01:43 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `conference`
--
CREATE DATABASE IF NOT EXISTS `conference` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `conference`;

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
CREATE TABLE IF NOT EXISTS `about` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date NOT NULL,
  `Venue` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`ID`, `Name`, `DateFrom`, `DateTo`, `Venue`) VALUES
(1, 'International Conference On Information Science & Communication Technology 2018', '2018-10-27', '2018-10-28', 'Arts Auditorium, University of Karachi');

-- --------------------------------------------------------

--
-- Table structure for table `about_para`
--

DROP TABLE IF EXISTS `about_para`;
CREATE TABLE IF NOT EXISTS `about_para` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_para`
--

INSERT INTO `about_para` (`ID`, `Text`, `Priority`) VALUES
(6, 'The Department of Computer Science, University of Karachi, was established by a resolution of Academic Council in its meeting, held on November 27. 1984, and it began functioning in the academic year 1985-86 by offering a Degree Program in Master of Computer Science (MCS) and become one of first institutions in Karachi imparting education in Computer Science and Technology. The Department also offers evening program leading to Post Graduate Diploma (PGD) in Computer & Information Sciences. In the year 1995; Department started MCS evening program, on self-finance basis, to cater the growing demand of professionally skilled manpower in the field of Computer Science.', 1),
(7, 'FCS at IBA endeavors to promote and uplift academic and organizational research activities in the discipline of computer science and information systems. Our aim is to provide a quality platform for acquiring, sharing, and disseminating results of completed and ongoing indigenous research activities. We hope to cultivate a strong tradition of applying state of the art research to the local contexts and solving local problems that can serve as global examples. To achieve this, we encourage and espouse collaborations with leading researchers around the world and keep exchange and growth of ideas in the prime focus.', 2),
(8, 'ICSIT is a biennial event and is entering its second decade of disseminating quality research in the area of Information and Communication Technologies. Topics of interest for submission include, but are not limited to artificial intelligence, robotics, social computing, wireless sensor networks, e-learning, computational science and other emerging topics. This year, the conference theme is "ICT in Everyday Life". We hope that you will enjoy the traditional Pakistani hospitality at ICSIT 2017, Karachi.', 3);

-- --------------------------------------------------------

--
-- Table structure for table `committee_groups`
--

DROP TABLE IF EXISTS `committee_groups`;
CREATE TABLE IF NOT EXISTS `committee_groups` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `committee_groups`
--

INSERT INTO `committee_groups` (`ID`, `Text`, `Priority`) VALUES
(1, 'Founders', 1),
(2, 'Management', 2),
(3, 'Media', 3);

-- --------------------------------------------------------

--
-- Table structure for table `committee_members`
--

DROP TABLE IF EXISTS `committee_members`;
CREATE TABLE IF NOT EXISTS `committee_members` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `committee_members`
--

INSERT INTO `committee_members` (`ID`, `GroupID`, `Name`, `Priority`) VALUES
(1, 1, 'Talha', 2),
(3, 1, 'Saad', 1),
(4, 1, 'Faheem', 3),
(5, 2, 'Irtaza', 1),
(6, 2, 'Kamran', 2),
(7, 3, 'Azib', 1),
(9, 1, 'Imran', 4);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `SentDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `papers_list`
--

DROP TABLE IF EXISTS `papers_list`;
CREATE TABLE IF NOT EXISTS `papers_list` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `papers_list`
--

INSERT INTO `papers_list` (`ID`, `Text`, `Priority`) VALUES
(15, 'Artificial Intelligence', 1),
(16, 'Data Mining', 2),
(17, 'Pattern Recognition & Image Processing', 3),
(18, 'Computer Graphics & Computer Vision', 4),
(19, 'Social Network Analysis', 5),
(20, 'Modeling and Simulation', 6),
(21, 'Computer Networks and Data Communication', 7),
(22, 'Network Security', 8),
(23, 'Ubiquitous Computing', 9),
(24, 'Wireless Communication & Mobile Computing', 10),
(25, 'Internet of IT', 11),
(26, 'Information Systems', 12),
(27, 'Database Systems', 13),
(28, 'Software Engineering & CASE', 14),
(29, 'Theoretical Computer Science', 15);

-- --------------------------------------------------------

--
-- Table structure for table `papers_para`
--

DROP TABLE IF EXISTS `papers_para`;
CREATE TABLE IF NOT EXISTS `papers_para` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `papers_para`
--

INSERT INTO `papers_para` (`ID`, `Text`, `Priority`) VALUES
(7, 'The international Conference on Information Sciences and Communication Technology (ICISCT) is the premier forum for the presentation of technological advances and research result in the field of theoretical experimental, and applied computer science and information technology. ICISCT 2018 will bring together leading scientist and engineers in the computer science and information technology from all around the world. Researchers are invited to submit their original, unpublished manuscript, which demonstrates current research in the area of Information Science and Technology.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_groups`
--

DROP TABLE IF EXISTS `sponsor_groups`;
CREATE TABLE IF NOT EXISTS `sponsor_groups` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsor_groups`
--

INSERT INTO `sponsor_groups` (`ID`, `Text`, `Priority`) VALUES
(1, 'Platinum Sponsors', 1),
(2, 'Gold Sponsors', 2),
(3, 'Silver Sponsors', 3);

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_items`
--

DROP TABLE IF EXISTS `sponsor_items`;
CREATE TABLE IF NOT EXISTS `sponsor_items` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsor_items`
--

INSERT INTO `sponsor_items` (`ID`, `GroupID`, `Logo`, `Priority`) VALUES
(2, 1, 'images/sponsors/2.png', 1),
(3, 2, 'images/sponsors/3.png', 2),
(4, 2, 'images/sponsors/4.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Username`, `Password`) VALUES
(1, '1', 'c4ca4238a0b923820dcc509a6f75849b'),
(2, 'master', 'eb0a191797624dd3a48fa681d3061212');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `about_para`
--
ALTER TABLE `about_para`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `committee_groups`
--
ALTER TABLE `committee_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `committee_members`
--
ALTER TABLE `committee_members`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `papers_list`
--
ALTER TABLE `papers_list`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `papers_para`
--
ALTER TABLE `papers_para`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sponsor_groups`
--
ALTER TABLE `sponsor_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sponsor_items`
--
ALTER TABLE `sponsor_items`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_para`
--
ALTER TABLE `about_para`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `committee_groups`
--
ALTER TABLE `committee_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `committee_members`
--
ALTER TABLE `committee_members`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `papers_list`
--
ALTER TABLE `papers_list`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `papers_para`
--
ALTER TABLE `papers_para`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sponsor_groups`
--
ALTER TABLE `sponsor_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sponsor_items`
--
ALTER TABLE `sponsor_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `committee_members`
--
ALTER TABLE `committee_members`
  ADD CONSTRAINT `committee_group_item` FOREIGN KEY (`GroupID`) REFERENCES `committee_groups` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sponsor_items`
--
ALTER TABLE `sponsor_items`
  ADD CONSTRAINT `sponsor_group_item` FOREIGN KEY (`GroupID`) REFERENCES `sponsor_groups` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
