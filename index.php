<?php

if (!isset($_SESSION))
	session_start();

require_once 'admin/functions.php';

$About = Search_Query("SELECT * from about", true);
$AboutParagraphs = Search_Query("SELECT * from about_para order by priority");
$PapersParagraphs = Search_Query("SELECT * from papers_para order by priority");
$Papers = Search_Query("SELECT * from papers_list order by priority");
$SponsorGroups = Search_Query("SELECT * from sponsor_groups order by priority");
$Speakers = Search_Query("SELECT * from speakers order by priority");

?>
<!DOCTYPE html>
<html lang="en" class="full-height">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title><?php echo $About['Name'] ?></title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		} 
	}
</style>
</head>
<body class="university">
	<header>
		<?php include 'nav.php'; ?>
		<section class="view" id="home">
			<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/ubit.jpg');">
				<div class="full-bg-img">
					<div id="particles" class="particles"></div>
					<div id="particles-c" class="particles particles-c"></div>
					<div class="container flex-center">
						<ul>
							<li>
								<div class="row smooth-scroll">
									<div class="col-md-12 white-text text-center">
										<div class="wow fadeInDown" data-wow-delay="0.2s">
											<h5 class="h5-responsive subtext-header mb-1 font-bold hidden-xs-down">GET READY</h5>
											<h1 class="h1-responsive font-bold mb-2"><?php echo $About['Name'] ?></h1>
											<hr class="hr-light w-50">
											<h3 class="h3-responsive subtext-header mt-4 mb-1"><?php echo date('j M', strtotime($About['DateFrom'])).' - '.date('j M Y', strtotime($About['DateTo'])) ?></h3>
											<h3 class="h3-responsive subtext-header mt-1 mb-5">Venue: <?php echo $About['Venue'] ?></h3>
										</div>
										<a class="btn btn-lg btn-outline-white wow fadeInUp waves-effect waves-light" data-wow-delay="0.2s">
											<span>Register Now</span>
										</a>
										<div class="row text-white text-center wow fadeIn mt-3" data-wow-delay="0.2s">
											<div class="col-xl-4 col-md-6 col-sm-12 offset-md-3 offset-xl-4">
												<div class="row">
													<div class="col-3 col-sm mb-2">
														<h1 class="green-text mb-1 font-bold timer-days">00</h1>
														<p>Days</p>
													</div>
													<div class="col-3 col-sm mb-2">
														<h1 class="green-text mb-1 font-bold timer-hours">00</h1>
														<p>Hours</p>
													</div>
													<div class="col-3 col-sm mb-2">
														<h1 class="green-text mb-1 font-bold timer-minutes">00</h1>
														<p>Minutes</p>
													</div>
													<div class="col-3 col-sm mb-2">
														<h1 class="green-text mb-1 font-bold timer-seconds">00</h1>
														<p>Seconds</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</section>
	</header>

	<main>
		<section id="about" class="section">
			<div class="container-fluid">
				<div class="container py-5 wow fadeIn" data-wow-delay="0.2s">
					<div class="divider-new mb-0">
						<h2 class="text-center font-up">ABOUT US</h2>
					</div>
					<p class="text-center font-up pb-3 font-bold">What is ICISCT</p>
					<?php 
					foreach ($AboutParagraphs as $Item)
						echo '<p align="justify">'.$Item['Text'].'</p>';
					?>
				</div>
			</div>
		</section>

		<section id="speakers" class="section grey lighten-3" style="border-top: 2px solid #b0bec5">
			<div class="container py-5 wow fadeIn" data-wow-delay="0.2s">
				<div class="divider-new mb-0">
					<h2 class="text-center font-up font-bold">Keynote Speakers</h2>
				</div>
				<p class="text-center font-up pb-3 font-bold">OUR KEYNOTE SPEAKERS</p>
				<div class="row mt-3">
					<?php 
					foreach ($Speakers as $Item)
					{
						?>
						<div class="col-lg-3 col-md-4 col-6 my-3">
							<div class="card">
								<div class="view overlay hm-zoom hm-black-strong">
									<img src="<?php echo $Item['Image'] ?>" class="img-fluid" alt="photo">
								</div>
								<div class="card-body">
									<p class="card-subtitle subtitle font-italic mb-0"><?php echo $Item['Organization'] ?></p>
									<p class="card-title mb-1"><b><a href="#" class="mdb-color-text"><?php echo $Item['Name'] ?></a></b></p>
									<p class="card-subtitle subtitle mb-2"><?php echo $Item['Education'] ?></p>
									<p class="card-text mb-0" style="font-size: 12px;"><?php echo substr($Item['Biography'], 0 ,160).''.(strlen($Item['Biography']) > 160 ? '...' : '') ?></p>
								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</section>

		<div class="streak streak-photo streak-sm hm-black-strong" data-jarallax='{"speed": 0.3}' style="background-image: url('images/bg-quote.jpg');">
			<div class="flex-center mask pattern-1">
				<div class="text-center white-text container">
					<h2 class="h2-responsive mb-5"><i class="fa fa-quote-left" aria-hidden="true"></i> We live in a society exquisitely dependent on science and technology, in which hardly anyone knows anything about science and technology. <i class="fa fa-quote-right" aria-hidden="true"></i></h2>
					<h5 class="text-center font-italic " data-wow-delay="0.2s">~ Carl Sagan</h5>
				</div>
			</div>
		</div>

		<section id="call-for-papers" class="section">
			<div class="container py-5">
				<div class="divider-new mb-0 pb-3">
					<h2 class="text-center font-up font-bold wow fadeIn" data-wow-delay="0.2s">Call for Papers</h2>
				</div>
				<?php 
				foreach ($PapersParagraphs as $Item)
					echo '<p align="justify">'.$Item['Text'].'</p>';
				?>
				<h5 class="font-bold mb-1">Areas Of Conference Theme</h5>
				<ul class="mt-2">
					<?php 
					foreach ($Papers as $Item)
						echo '<li>'.$Item['Text'].'</li>';
					?>
				</ul>
			</div>
		</section>

		<section id="registration" class="section grey lighten-3" style="border-top: 2px solid #b0bec5">
			<div class="container py-5 wow fadeIn" data-wow-delay="0.2s">
				<div class="divider-new mb-0 pb-3">
					<h2 class="text-center font-up font-bold">Registration</h2>
				</div>
				<h5 class="font-bold mb-1">Important Dates</h5>
				<p class="mb-1">Paper submission: June 30, 2018</p>
				<p class="mb-1">Acceptance Notification: </p>
				<p class="mb-1">Camera Ready Papers:</p>
				<p class="mb-1">Registration Dead line</p>

				<table class="table table-bordered white mt-3">
					<thead>
						<tr>
							<th colspan="2" class="text-center"><strong>Category</strong></th>
							<th><strong>Fees</strong></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th rowspan="3" class="text-center"><strong>Author</strong></th>
							<th>Faculty</th>
							<th>4,000 PKR</th>
						</tr>
						<tr>
							<th>Student</th>
							<th>2,000 PKR</th>
						</tr>
						<tr>
							<th>International</th>
							<th>200 USD</th>
						</tr>
						<tr>
							<th rowspan="3" class="text-center"><strong>Participants</strong></th>
							<th>Faculty</th>
							<th>2,000 PKR</th>
						</tr>
						<tr>
							<th>Student</th>
							<th>1,500 PKR</th>
						</tr>
						<tr>
							<th>International</th>
							<th>100 USD</th>
						</tr>
					</tbody>
				</table>
				<div class="text-center">
					<a class="btn btn-lg btn-indigo wow fadeInUp waves-effect waves-light" data-wow-delay="0.2s">
						<span>Register Now</span>
					</a>
				</div>
			</div>
		</section>

		<div class="streak streak-photo streak-long-2 hm-black-strong" data-jarallax='{"speed": 0.5}' style="background-image: url('images/stats.jpg');">
			<div class="mask flex-center">
				<div class="container">
					<h1 class="text-center green-text mb-2 font-up font-bold wow fadeIn" data-wow-delay="0.2s">Hurry Up!</h1>
					<p class="text-center text-white mb-5 font-up font-bold wow fadeIn" data-wow-delay="0.2s">Last year our tickets sold out. Act fast so you don’t miss out. ICISCT will start in</p>
					<div class="row text-white text-center wow fadeIn" data-wow-delay="0.2s">
						<div class="col-xl-4 col-md-6 col-sm-12 offset-md-3 offset-xl-4">
							<div class="row">
								<div class="col-3 col-sm mb-2">
									<h1 class="green-text mb-1 font-bold timer-days">00</h1>
									<p>Days</p>
								</div>
								<div class="col-3 col-sm mb-2">
									<h1 class="green-text mb-1 font-bold timer-hours">00</h1>
									<p>Hours</p>
								</div>
								<div class="col-3 col-sm mb-2">
									<h1 class="green-text mb-1 font-bold timer-minutes">00</h1>
									<p>Minutes</p>
								</div>
								<div class="col-3 col-sm mb-2">
									<h1 class="green-text mb-1 font-bold timer-seconds">00</h1>
									<p>Seconds</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section id="sponsors" class="section mb-2">
			<div class="container py-5 wow fadeIn" data-wow-delay="0.2s">
				<div class="divider-new mb-4">
					<h2 class="text-center font-up font-bold">Sponsors</h2>
				</div>
				<?php
				foreach ($SponsorGroups as $Group)
				{
					$Sponsors = Search_Query("SELECT * from sponsor_items where groupid = '".$Group['ID']."' order by priority");
					if (empty($Sponsors))
						continue;
					?>
					<h4 class="deep-orange-text"><?php echo $Group['Text'] ?></h4>
					<div class="row wow fadeIn flex-center" data-wow-delay="0.2s">
						<?php
						foreach ($Sponsors as $Sponsor)
						{
							?>
							<div class="col-md-3 col-sm-4 col-6">
								<img src="<?php echo $Sponsor['Logo'] ?>" class="img-fluid px-5 py-4">
							</div>
							<?php
						}
						?>
					</div>
					<?php
				}
				?>
			</div>
		</section>

		<section id="venue-map" class="section mb-2">
			<div class="container divider-new mb-4">
				<h2 class="text-center font-up font-bold">Venue Map</h2>
			</div>
			<div id="map-container" class="z-depth-1" style="height: 500px"></div>
		</section>

		<section id="contact" class="section mb-2">
			<div class="container py-5 wow fadeIn" data-wow-delay="0.2s">
				<div class="divider-new mb-4">
					<h2 class="text-center font-up font-bold">Contact Us</h2>
				</div>			
				<div class="row wow fadeIn" data-wow-delay="0.2s">
					<div class="col-md-8">
						<form method="post" id="form-contact-us">
							<div class="row">
								<div class="col-md-6">
									<div class="md-form">
										<div class="md-form">
											<input type="text" name="name" id="name" class="form-control">
											<label for="name" class="">Your name</label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="md-form">
										<div class="md-form">
											<input type="email" name="email" id="email" class="form-control">
											<label for="email" class="">Your email</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form">
										<input type="text" name="subject" id="subject" class="form-control" required>
										<label for="subject" class="">Subject</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form">
										<textarea type="text" name="message" id="message" class="md-textarea" required></textarea>
										<label for="message">Your message</label>
									</div>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-rounded btn-outline-black waves-effect" id="btn-send-message"><i class="fa fa-send mr-2"></i> Send</button>
							</div>
						</form>
					</div>
					<div class="col-md-4 mt-4 mt-md-0">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-12">
								<ul class="contact-icons">
									<li><i class="fa fa-map-marker fa-2x"></i>
										<p class="grey-text-555">UBIT (UoK) Karachi – 75270, Pakistan</p>
									</li>
								</ul>
							</div>
							<div class="col-12 col-sm-4 col-md-12">
								<ul class="contact-icons">
									<li><i class="fa fa-phone fa-2x"></i>
										<p class="grey-text-555">993261300-7 Ext: 2462</p>
									</li>
								</ul>
							</div>
							<div class="col-12 col-sm-4 col-md-12">
								<ul class="contact-icons">
									<li><i class="fa fa-envelope fa-2x"></i>
										<p class="grey-text-555 mb-1">www.uok.edu.pk/icisct</p>
										<p class="grey-text-555">icisct@uok.edu.pk</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/jquery.particleground.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDWxwZDuoJJAtW27E90N7sp6AnNsXvSuk0"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			function regular_map() {
				var var_location = new google.maps.LatLng(24.9376766, 67.1200188);

				var var_mapoptions = {
					center: var_location,
					zoom: 15
				};

				var var_map = new google.maps.Map(document.getElementById("map-container"),
					var_mapoptions);

				var var_marker = new google.maps.Marker({
					position: var_location,
					map: var_map,
					title: "New York"
				});
			}

			google.maps.event.addDomListener(window, 'load', regular_map);

			$('.mdb-select').material_select();

			$("a").on('click', function(event) {
				if (this.hash !== "") {
					var hash = this.hash;
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 700, function(){      	
						window.location.hash = hash;
					});
				}
			});


			$("#form-contact-us").submit(function(e) {
				e.preventDefault();
				$("#btn-send-message").prop('disabled', 'disabled')
				$("#btn-send-message").html('<i class="fa fa-spinner fa-spin mr-2"></i> Sending');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "sendmessage.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Message sent successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-contact-us")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#btn-send-message").prop('disabled', '')
						$("#btn-send-message").html('<i class="fa fa-send mr-2"></i> Send');
					}
				});
			});
		});

		$("#particles").particleground({
			minSpeedX: .6,
			minSpeedY: .6,
			dotColor: "#ffffff",
			lineColor: "#ffffff",
			density: 8e3,
			particleRadius: 3,
			parallaxMultiplier: 5.2,
			proximity: 0
		});

		var countDownDate = new Date("<?php echo date('j M, Y', strtotime($About['DateFrom'])).' '.date('H:i:s', strtotime($About['StartTime'])) ?>").getTime();
		var x = setInterval(function() {

			var now = new Date().getTime();
			var distance = countDownDate - now;

			if (distance >= 0) 
			{
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				days = (days < 10 ? '0'+days : days);
				hours = (hours < 10 ? '0'+hours : hours);
				minutes = (minutes < 10 ? '0'+minutes : minutes);
				seconds = (seconds < 10 ? '0'+seconds : seconds);

				$('.timer-days').html(days);
				$('.timer-hours').html(hours);
				$('.timer-minutes').html(minutes);
				$('.timer-seconds').html(seconds);
			}
		}, 1000);
	</script>
</body>
</html>