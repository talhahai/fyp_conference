<?php

if (!isset($_SESSION))
	session_start();

if (isset($_SESSION['userid']) && isset($_SESSION['token']) && isset($_SESSION['name']) && isset($_SESSION['type']))
{
	echo '<span id="user-data" class="hidden-xl-up" data-userid="'.$_SESSION['userid'].'" data-token="'.$_SESSION['token'].'" data-name="'.$_SESSION['name'].'" data-type="'.$_SESSION['type'].'"></span>';
}

?>

<footer class="page-footer center-on-small-only mt-0 mdb-color darken-3 pt-0">
	<div class="footer-copyright">
		<div class="container-fluid">
			© 2017 Copyright: <a href="http://www.MDBootstrap.com"> WebDroid Solutions </a>
		</div>
	</div>
</footer>